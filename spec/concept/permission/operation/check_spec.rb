require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Project::Operation::Create, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {} }
  let(:expected_attrs) { {} }

  Permission.create(resource_class: "Project", action: "create", resource_id: 42, uid: "3A", allowed?: true)
  Permission.create(resource_class: "Client", action: "update", resource_id: 13, uid: "3A", allowed?: true)
  Permission.create(resource_class: "Project", action: "index", uid: "3A", allowed?: true)
  Permission.create(resource_class: "Project", action: "index", uid: "4", allowed?: false)
  Permission.create(resource_class: "Client", action: "show", resource_id: 13, uid: "3A", allowed?: true)
  Permission.create(resource_class: "Client", action: "show", resource_id: 14, uid: "3A", allowed?: false)
  Permission.create(resource_class: "Project", action: "delete", resource_id: 13, uid: "3A", allowed?: true)


  it "create" do
    params = {resource_class: "Project", action: "create", resource_id: 43, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Project", action: "create", uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Project", action: "create", uid: "XX"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Project-777", action: "create", uid: "XX"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?
  end

  it "show" do
    params = {resource_class: "Project", action: "show", resource_id: 13, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Client", action: "show", resource_id: 13, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Client", action: "show", resource_id: 14, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Client", action: "show", uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?
  end

  it "update" do
    params = {resource_class: "Client", action: "update", resource_id: 13, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Client", action: "update", resource_id: 14, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Client", action: "update", resource_id: 13, uid: "3b"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?
  end

  it "index" do
    params = {resource_class: "Project", action: "index", uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Project", action: "index", resource_id: 13, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Project", action: "index", uid: "4"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Project", action: "index", resource_id: 13, uid: "4"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?
  end

  it "delete" do
    params = {resource_class: "Project", action: "delete", resource_id: 13, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert result.success?

    params = {resource_class: "Project-XXX", action: "delete", resource_id: 13, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Project", action: "delete", resource_id: 14, uid: "3A"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?

    params = {resource_class: "Project", action: "delete", resource_id: 13, uid: "3b"}
    result = Permission::Operation::Check.call(params: params)
    assert !result.success?
  end

end
