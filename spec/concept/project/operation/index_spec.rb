require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Project::Operation::Index, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }


  it "index" do
    Project.delete_all
    (0..15).each {|i| Project.create(name: "name#{i}")}
    result = Project::Operation::Index.call(params: {}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_equal result["model"].pluck(:name), ["name0", "name1", "name2", "name3", "name4", "name5", "name6", "name7", "name8", "name9"]
    result2 = Project::Operation::Index.call(params: {page: 2}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result2.success?
    assert_equal result2["model"].pluck(:name), ["name10", "name11", "name12", "name13", "name14", "name15"]
  end

end
