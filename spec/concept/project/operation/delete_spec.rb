require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Project::Operation::Delete, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }

  it "delete" do
    project = Project.create(name: "client")
    result = Project::Operation::Delete.call(params: {id: project.id}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_nil Project.find_by(id: project.id)
  end

end
