require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Project::Operation::Update, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }

  it "update" do
    project = Project.create(name: "project")
    result = Project::Operation::Update.call(params: {id: project.id, name: "edited", status: 2}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    project.reload
    assert result.success?
    assert_equal result["model"].attributes, project.attributes
  end

end
