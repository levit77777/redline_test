require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Project::Operation::Show, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }


  it "show" do
    client = Client.create(name: "client")
    project = Project.create(name: "project", client: client )
    result = Project::Operation::Show.call(params: {id: project.id}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_equal result["model"].attributes, project.attributes
  end

end
