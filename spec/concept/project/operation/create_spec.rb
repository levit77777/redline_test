require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

(1..10).each { |i| Permission.create(uid: "1", resource_class: "Project", action: "show", resource_id: i, allowed?: true)}
(1..10).each { |i| Permission.create(uid: "1", resource_class: "Project", action: "update", resource_id: i, allowed?: true)}
(1..10).each { |i| Permission.create(uid: "1", resource_class: "Project", action: "delete", resource_id: i, allowed?: true)}
Permission.create(uid: "1", resource_class: "Project", action: "index", allowed?: true)
Permission.create(uid: "1", resource_class: "Project", action: "create", allowed?: true)

(1..10).each { |i| Permission.create(uid: "1", resource_class: "Client", action: "show", resource_id: i, allowed?: true)}
(1..10).each { |i| Permission.create(uid: "1", resource_class: "Client", action: "update", resource_id: i, allowed?: true)}
(1..10).each { |i| Permission.create(uid: "1", resource_class: "Client", action: "delete", resource_id: i, allowed?: true)}
Permission.create(uid: "1", resource_class: "Client", action: "index", allowed?: true)
Permission.create(uid: "1", resource_class: "Client", action: "create", allowed?: true)


RSpec.describe Project::Operation::Create, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }

  it "create" do
    Client.destroy_all
    result = Project::Operation::Create.call(params: {name: "created", client: {name: "for project"}}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_equal result["model"].attributes, Project.last.attributes
    assert_equal result["model"].client.attributes, Client.last.attributes
  end

  it "create_with_existed_client" do
    Client.destroy_all
    client = Client.create(name: "for project")
    result = Project::Operation::Create.call(params: {name: "created", client: {name: "for project"}}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_equal result["model"].attributes, Project.last.attributes
    assert_equal result["model"].client.attributes, client.attributes
  end

  it "create_with_client_model" do
    Client.destroy_all
    client = Client.create(name: "for project")
    result = Project::Operation::Create.call(params: {name: "created", client: client}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_equal result["model"].attributes, Project.last.attributes
    assert_equal result["model"].client.attributes, client.attributes
  end

  it "create_fail" do
    Client.destroy_all
    Project.destroy_all
    result = Project::Operation::Create.call(params: {name: "1234567890123456", client: {name: "for project"}}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert !result.success?
    assert_nil Project.last
    assert_nil Client.last
  end

  it "transaction_rollback" do
    Client.destroy_all
    Project.destroy_all
    result = Project::Operation::Create.call(params: {name: "created", client: {name: "1234567890123456"}}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert !result.success?
    assert_nil Project.last
    assert_nil Client.last
  end

end
