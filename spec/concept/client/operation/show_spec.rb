require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Client::Operation::Show, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }


  it "show" do
    Client.create(name: "client")
    client = Client.last
    result = Client::Operation::Show.call(params: {id: client.id}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_equal result["model"].attributes, client.attributes
  end

end
