require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Client::Operation::Delete, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }

  it "delete" do
    client = Client.create(name: "client")
    project1 = Project.create(name: "project1", client: client)
    project2 = Project.create(name: "project2", client: client)
    result = Client::Operation::Delete.call(params: {id: client.id}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert result.success?
    assert_nil Client.find_by(id: client.id)
    assert_nil Project.find_by(id: project1.id)
    assert_nil Project.find_by(id: project2.id)
  end

end
