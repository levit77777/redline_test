require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Client::Operation::Update, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }

  it "update" do
    client = Client.create(name: "client")
    result = Client::Operation::Update.call(params: {id: client.id, name: "edited"}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    client.reload
    assert result.success?
    assert_equal result["model"].attributes, client.attributes
  end

end
