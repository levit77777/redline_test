require 'rails_helper'
require "trailblazer/test/deprecation/operation/assertions"

RSpec.describe Client::Operation::Create, type: :class do
  include Trailblazer::Test::Deprecation::Operation::Assertions # in your test class
  let(:default_params) { {name: "name"} }
  let(:expected_attrs) { {name: "name"} }

  it "create" do
    result = Client::Operation::Create.call(params: {name: "created"}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    client = Client.last
    assert result.success?
    assert_equal result["model"].attributes, client.attributes
  end

  it "create_with_projects" do
    result = Client::Operation::Create.call(params: {name: "created", projects: [{name: "project1"}, {name: "project2"}]}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    client = Client.last
    assert result.success?
    assert_equal result["model"].attributes, client.attributes
    assert_equal client.projects.pluck(:name) , ["project1", "project2"]
  end

  it "create_fail" do
    result = Client::Operation::Create.call(params: {name: "1234567890123456"}, headers: {"Token" => "eyJhbGciOiJIUzI1NiJ9.IjEi.ROxtokzoYB5uNUBvAz1ynnnfTytWxesdXx2d_bU1saw"})
    assert !result.success?
    assert_nil Client.last
  end

end
