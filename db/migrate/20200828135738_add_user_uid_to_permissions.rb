class AddUserUidToPermissions < ActiveRecord::Migration[6.0]
  def change
    add_column :permissions, :uid, :string
  end
end
