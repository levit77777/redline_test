class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string :name, empty: false
      t.integer :status, default: 0
      t.integer :client_id

      t.timestamps
    end
  end
end
