class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.string :resource_class
      t.string :action
      t.integer :resource_id
      t.boolean :allowed?

      t.timestamps
    end
  end
end
