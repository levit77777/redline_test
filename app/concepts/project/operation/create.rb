module Project::Operation
  class Create < Base

    step Model(Project, :new)
    step Contract::Build(constant: ::Project::Contract::Create)
    step Wrap( Transaction ) {
      step :find_or_create_client
      step :add_client
      step Contract::Validate()
      step Contract::Persist()
      fail :rollback
    }
    fail :catch_validation_errors

    def find_or_create_client(ctx, **)
      params = ctx[:params]
      headers = ctx[:headers]
      ctx["client"] = params[:client] if params[:client].is_a?(Client)
      ctx["client"] ||= Client.find_by(name: params.dig(:client,:name))
      ctx["client"] ||= Client::Operation::Create.call(params: {name: params.dig(:client,:name)}, headers: headers)[:model]
    end

    def add_client(ctx, **)
      ctx[:"contract.default"].client_id = ctx["client"].id
    end

    def rollback(ctx, options)
      raise ActiveRecord::Rollback
    end

    def catch_validation_errors(ctx, options)
      ctx[:error] = options[:"contract.default"] ? options[:"contract.default"].errors.messages : "error"
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Project", action: "create"}
      super(ctx, options.merge(permission_params: permission_params))
    end

  end
end