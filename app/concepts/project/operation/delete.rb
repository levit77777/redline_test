module Project::Operation
  class Delete < Base
    step Model(Project, :find)
    step :delete!

    def delete!(ctx, model:, **)
      model.destroy
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Project", action: "delete", resource_id: ctx[:params][:id]}
      super(ctx, options.merge(permission_params: permission_params))
    end
  end
end
