module Project::Operation
  class Update < Base
    step Model(Project, :find)
    step Contract::Build(constant: Project::Contract::Create)
    step Contract::Validate()
    step Contract::Persist()
    fail :catch_validation_errors

    def catch_validation_errors(ctx, options)
      ctx[:error] = options[:"contract.default"].errors.messages
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Project", action: "update", resource_id: ctx[:params][:id]}
      super(ctx, options.merge(permission_params: permission_params))
    end
  end

end
