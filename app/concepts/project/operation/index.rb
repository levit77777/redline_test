module Project::Operation
  class Index < Base
    step :model!

    def model!(ctx, **)
      params = ctx[:params]
      ctx[:model] = Project.page(params[:page]).includes(:client)
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Project", action: "index"}
      super(ctx, options.merge(permission_params: permission_params))
    end
  end
end