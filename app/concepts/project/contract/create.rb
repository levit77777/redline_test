require 'reform/form/dry'

module Project::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry
    property :name
    property :client_id
    property :status

    validation  do
      params do
        required(:name).maybe(max_size?: 15)
      end
    end

  end
end