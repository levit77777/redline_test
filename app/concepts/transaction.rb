class Transaction
  def self.call((ctx, flow_options), *, &block)
    ActiveRecord::Base.transaction { yield } # calls the wrapped steps
  end
end