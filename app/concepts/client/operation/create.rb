module Client::Operation
  class Create < Base

    step Model(Client, :new)
    step Contract::Build(constant: Client::Contract::Create)
    step Contract::Validate()
    step Wrap( Transaction ) {
      step Contract::Persist()
      step :create_projects
      fail :rollback
    }
    fail :catch_validation_errors

    def create_projects(ctx, options)
      headers = ctx[:headers]
      project = ctx[:params][:projects] || []
      success = project.map do |p|
        Project::Operation::Create.call(params: p.merge(client: ctx[:model]), headers: headers)
      end
      success.all?{|i| i.success?}
    end

    def rollback(ctx, options)
      raise ActiveRecord::Rollback
    end

    def catch_validation_errors(ctx, options)
      ctx[:error] = options[:"contract.default"] ? options[:"contract.default"].errors.messages : "error"
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Client", action: "create"}
      super(ctx, options.merge(permission_params: permission_params))
    end

  end
end
