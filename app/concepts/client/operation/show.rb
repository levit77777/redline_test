module Client::Operation
  class Show < Base
    step Model(Client, :find)

    def check_permission(ctx, options)
      permission_params = {resource_class: "Client", action: "show", resource_id: ctx[:params][:id]}
      super(ctx, options.merge(permission_params: permission_params))
    end
  end

end
