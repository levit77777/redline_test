module Client::Operation
  class Update < Base

    step Model(Client, :find)
    step Contract::Build(constant: Client::Contract::Create)
    step Contract::Validate()
    step Contract::Persist()
    fail :catch_validation_errors

    def catch_validation_errors(ctx, options)
      ctx[:error] = options[:"contract.default"].errors.messages
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Client", action: "update", resource_id: ctx[:params][:id]}
      super(ctx, options.merge(permission_params: permission_params))
    end

  end
end
