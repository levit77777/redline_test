module Client::Operation
  class Index < Base
    step :model!

    def model!(ctx, **)
      params = ctx[:params]
      ctx[:model] = Client.page(params[:page]).includes(:projects)
    end

    def check_permission(ctx, options)
      permission_params = {resource_class: "Client", action: "index"}
      super(ctx, options.merge(permission_params: permission_params))
    end

  end
end
