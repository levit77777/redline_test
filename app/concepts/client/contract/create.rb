require 'reform/form/dry'

module Client::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry
    property :name
    validation  do
      params do
        #required(:name).value(:filled?)
        required(:name).maybe(max_size?: 15)
      end
    end
  end
end
