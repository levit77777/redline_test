class Base < Trailblazer::Operation

  step :check_permission
  fail :unpermitted_request_handling , fail_fast: true

  def check_permission(ctx, options)
    begin
      token = ctx[:headers]["Token"]
      hmac_secret = ENV['hmac_secret']
      uid = JWT.decode(token, hmac_secret).first
      permission_params = options[:permission_params].merge(uid: uid)
      permission_request = Permission::Operation::Check.call(params: permission_params)
      permission_request.success?
    rescue
      false
    end
  end

  def unpermitted_request_handling(ctx, options)
    ctx[:model] = "unpermitted request by this token"
    ctx[:error] = ctx[:model]
    ctx[:status] = 401
  end

end

