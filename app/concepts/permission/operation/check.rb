class Permission::Operation::Check < Trailblazer::Operation
  IDS_ACTIONS = %w(show update delete)
  step :model!

  def model!(ctx, **)
    params = ctx[:params]
    permissions = Permission.where(resource_class: params[:resource_class],
                                    action: params[:action],
                                    uid: params[:uid])
    permissions = permissions.where(resource_id: params[:resource_id]) if IDS_ACTIONS.include?(params[:action])
    permission = permissions.first
    ctx[:render] = permission ? permission.allowed? : false
  end

end
