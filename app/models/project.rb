class Project < ApplicationRecord
  enum status: [:created, :active, :archived ]
  belongs_to :client, optional: true

end
