module V1
  class ClientSerializer < ActiveModel::Serializer
    attributes :id, :name
    has_many :projects, serializer: ProjectSerializer
  end
end
