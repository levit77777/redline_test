module V1
  class ProjectSerializer < ActiveModel::Serializer
    attributes :id, :name, :status, :created_at
    belongs_to :client, serializer: ClientSerializer
  end
end
