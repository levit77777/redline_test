class API < Grape::API
  prefix :api
  format :json
  formatter :json, Grape::Formatter::ActiveModelSerializers

  mount V1::API

end