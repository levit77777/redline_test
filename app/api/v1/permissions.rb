module V1
  class Permissions < Grape::API
    get '/check_permission' do
      token = headers["Token"]
      hmac_secret = ENV['hmac_secret']
      uid = JWT.decode(token, hmac_secret).first

      permission_request = Permission::Operation::Check.call(params: params.merge(uid: uid))
      status(permission_request.success? ? 200 : 401)
    end
  end
end
