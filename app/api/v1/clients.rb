module V1
  class Clients < Grape::API

    resource :clients do
      get '' do
        result = Client::Operation::Index.call(params: params, headers: headers)
        status(result[:status]) if result[:status]
        result[:model]
      end

      get ':id' do
        result = Client::Operation::Show.call(params: params, headers: headers)
        status(result[:status]) if result[:status]
        result[:model]
      end

      post do
        result = Client::Operation::Create.call(params: params, headers: headers)
        if result.success?
          status(201)
          result[:model]
        else
          status(result[:status] || 422)
          result[:error]
        end
      end

      put ':id' do
        result = Client::Operation::Update.call(params: params, headers: headers)
        if result.success?
          status(200)
          result[:model]
        else
          status(result[:status] || 422)
          result[:error]
        end
      end

      delete ':id' do
        result = Client::Operation::Delete.call(params: params, headers: headers)
        status(result.success? ? 204 : 405)
        status(result[:status]) if result[:status]
      end
    end

  end
end
