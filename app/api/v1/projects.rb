module V1
  class Projects < Grape::API

    resource :projects do
      get '' do
        result = Project::Operation::Index.call(params: params, headers: headers)
        status(result[:status]) if result[:status]
        result[:model]
      end

      get ':id' do
        result = Project::Operation::Show.call(params: params, headers: headers)
        status(result[:status]) if result[:status]
        result[:model]
      end

      post do
        result = Project::Operation::Create.call(params: params, headers: headers)
        if result.success?
          status(201)
          result[:model]
        else
          status(result[:status] || 422)
          result[:error]
        end
      end

      put ':id' do
        result = Project::Operation::Update.call(params: params, headers: headers)
        if result.success?
          status(200)
          result[:model]
        else
          status(result[:status] || 422)
          result[:error]
        end
      end

      delete ':id' do
        result = Project::Operation::Delete.call(params: params, headers: headers)
        status(result.success? ? 204 : 422)
        status(result[:status]) if result[:status]
      end
    end

  end
end
